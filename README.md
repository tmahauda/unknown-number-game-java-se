# Unknown Number Game

<div align="center">
<img width="500" height="300" src="inconnu.jpg">
</div>

## Description du projet

Application console réalisée avec Java SE en MASTER INFO 1 à l'université d'Angers dans le cadre du module "Programmation réseaux" durant l'année 2019-2020. \
Il s'agit de proposer une paire de programmes client-serveur en ligne de commandes permettant de jouer massivement au jeu du nombre inconnu.

## Acteurs

### Réalisateurs

Ce projet a été réalisé par :
- Théo MAHAUDA : tmahauda@etud.univ-angers.fr.

### Encadrants

Ce projet fut encadré par un enseignant de l'unversité d'Angers :
- Benoît DA MOTA : benoit.da-mota@univ-angers.fr.

## Organisation

Ce projet a été agit au sein de l'université d'Angers dans le cadre du module "Programmation réseaux" du MASTER INFO 1.

## Date de réalisation

Ce projet a été éxécuté durant l'année 2020 sur la période du confinement COVID-19 à la maison au mois de Mars-Avril. \
Il a été terminé et rendu le 09/04/2020.

## Technologies, outils et procédés utilisés

Ce projet a été accomplis avec les technologies, outils et procédés suivants :
- Java ;
- Eclipse ;
- Maven ;
- Sockets TCP ;
- Thread.

## Objectif

### Règles du jeu
- Le serveur tire aléatoirement un nombre entre 1 et 1 000 000 sans le révéler aux joueurs ;
- Chaque joueur présent peut faire jusqu'à 3 propositions, après quoi il devra attendre la fin de la partie. Si aucun joueur présent ne peut jouer (aucune proposition disponible) le serveur
relance une nouvelle partie ;
- À chaque proposition, le serveur envoi la proposition à tous les joueurs ainsi que l'intervalle mise à jour dans laquelle se trouve le nombre mystère (cf. exemple).

### Aspect réseau
- Le serveur gère une salle unique où les joueurs jouent en temps réel (quand ils veulent) ;
- Quand un nouveau joueur arrive, il entre automatiquement dans la partie en cours. Un message avertira les autres joueurs de son arrivée ;
- Il existe un classement du serveur où on comptera le nombre de parties gagnées par chaque joueur ;
- Un joueur peut lancer des commandes, notamment :
  - Pour faire une proposition ;
  - Pour afficher l'état de la partie en cours, qu'il sera le seul à voir ;
  - Pour afficher le classement serveur, qu'il sera le seul à voir ;
  - Pour quitter, ce qui avertira les autres joueurs de son départ ;
  - Pour afficher l'aide.
- Les messages en dehors du protocole sont simplement ignorés.

### Exemple

Exemple d'affichage sur le client de Bob : 
- [serveur] Bienvenue sur le serveur du nombre inconnu Bob !
- [serveur] Vous pouvez taper /aide pour obtenir la liste des commandes valides et les règles du jeu. [serveur] Le nombre inconnu est compris en 1 et 1000000.
- [serveur] Alice vient de se connecter.
- [Bob] 500000
- [serveur>bob] Il reste 2 essai(s).
- [serveur] Bob propose 500000, le nombre est plus grand
- [serveur] Le nombre inconnu est compris en 500000 et 1000000.
- [Bob] 750000
- [serveur>bob] Il reste 1 essai(s).
- [serveur] Bob propose 750000, le nombre est plus petit
- [serveur] Le nombre inconnu est compris en 500000 et 750000.
- [serveur] Alice propose 625000, le nombre est plus grand
- [serveur] Le nombre inconnu est compris en 625000 et 750000.
- [Bob] /partie
- [serveur>bob] Le nombre inconnu est compris en 625000 et 750000.
- [serveur>bob] Il reste 1 essai(s) à Bob
- [serveur>bob] Il reste 2 essai(s) à Alice
- [serveur] Alice vient de se déconnecter.
- [Bob] 687500
- [serveur>bob] Il reste 0 essai(s).
- [serveur] Bob propose 687500, le nombre est plus petit
- [serveur] Plus aucun joueur ne peut jouer, fin de partie
- [serveur] Le nombre inconnu était 666666
- [serveur] Début de nouvelle partie
- [serveur] Le nombre inconnu est compris en 1 et 1000000.
- [serveur>bob] Il reste 3 essai(s).
- [Bob] /classement
- [serveur>bob] classement du serveur :
- [serveur>bob] Eve : 5 victoire(s)
- [serveur>bob] Bob : 2 victoire(s)
- [serveur>bob] Alice : 0 victoire(s)
- [Bob] /quitter
- [serveur>bob] À bientôt Bob !