package universite.angers.master.info.unknown.number.game.server.command.request;

import universite.angers.master.info.network.service.Commandable;
import universite.angers.master.info.unknown.number.game.server.ServerPlayer;
import universite.angers.master.info.unknown.number.game.server.ServerUnknownNumberGame;

/**
 * Classe qui permet de renvoyer la liste des commandes valides et les règles du jeu
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ServerRequestHelp implements Commandable<ServerPlayer> {

	private ServerPlayer player;
	
	@Override
	public boolean send(ServerPlayer player) {
		//Si aucune instance du joueur
		if(player == null) return false;

		//On récupère le joueur coté serveur
		this.player = ServerUnknownNumberGame.getInstance().getOrNewPlayer(player.getIdentifiant());
		
		//On lui affiche la liste des commandes valides
		
		this.player.getMessages().add("Voici la liste des commandes valides : ");
		for(String command : ServerUnknownNumberGame.getInstance().getServer().
				getServerService().getRequests().keySet()) {
			this.player.getMessages().add(command);
		}
		
		//On lui affiche les règles du jeu
		
		this.player.getMessages().add("Voici les règles : ");
		this.player.getMessages().add("Le serveur tire aléatoirement un nombre entre 1 et 1 000 000 sans le révéler aux joueurs");
		this.player.getMessages().add("Chaque joueur présent peut faire jusqu'à 3 propositions, après quoi il devra attendre la fin de\n" + 
				"la partie. Si aucun joueur présent ne peut jouer (aucune proposition disponible) le serveur\n" + 
				"relance une nouvelle partie");
		this.player.getMessages().add("À chaque proposition, le serveur envoi la proposition à tous les joueurs ainsi que l'intervalle\n" + 
				"mise à jour dans laquelle se trouve le nombre mystère");

		return true;
	}

	@Override
	public ServerPlayer receive(Object arg) {
		//On renvoit le joueur modifié dans la méthode "send"
		this.player.setCommand("REQUEST_HELP");
		return this.player;
	}
}
