package universite.angers.master.info.unknown.number.game.server;

/**
 * Test serveur
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ServerMain {

	public static void main(String[] args) {
		ServerUnknownNumberGame.getInstance().getServer().open();
	}
}
