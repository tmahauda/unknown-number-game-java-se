package universite.angers.master.info.unknown.number.game.server.command.request;

import universite.angers.master.info.network.server.Subject;
import universite.angers.master.info.network.service.Commandable;
import universite.angers.master.info.unknown.number.game.server.ServerPlayer;
import universite.angers.master.info.unknown.number.game.server.ServerUnknownNumberGame;

/**
 * Classe qui permet de renvoyer l'état du jeu
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ServerRequestName implements Commandable<ServerPlayer> {

	private ServerPlayer player;
	
	@Override
	public boolean send(ServerPlayer player) {
		//Si aucune instance du joueur
		if(player == null) return false;

		// On profite pour enregistrer le joueur
		this.player = ServerUnknownNumberGame.getInstance().getOrNewPlayer(player.getIdentifiant());
		
		//On lui affecte le nom
		this.player.setName(player.getName());

		// On souhaite la bienvenu au client
		this.player.getMessages().add("Bienvenue sur le serveur du nombre inconnu " + this.player.getName());
		this.player.getMessages().add("Vous pouvez taper /help pour obtenir la liste des commandes valides et les règles du jeu");
		
		//On informe les autres clients d'un nouveau client s'est connecté
		ServerUnknownNumberGame.getInstance().getServer().notifyAllObservers(Subject.NOTIFY_NEW.getName(), this.player);

		return true;
	}

	@Override
	public ServerPlayer receive(Object arg) {
		//On renvoit le joueur modifié dans la méthode "send"
		this.player.setCommand("REQUEST_NAME");
		return this.player;
	}
}
