package universite.angers.master.info.unknown.number.game.server.command.request;

import universite.angers.master.info.network.service.Commandable;
import universite.angers.master.info.unknown.number.game.server.ServerPlayer;
import universite.angers.master.info.unknown.number.game.server.ServerUnknownNumberGame;

/**
 * Classe qui permet de vérifier si la proposition du joueur est bonne ou non du nombre mystère
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ServerRequestOffer implements Commandable<ServerPlayer> {

	private ServerPlayer player;
	
	@Override
	public boolean send(ServerPlayer player) {
		//Si aucune instance du joueur
		if(player == null) return false;

		//On récupère le joueur coté serveur
		this.player = ServerUnknownNumberGame.getInstance().getOrNewPlayer(player.getIdentifiant());
		
		//On récupère l'offre du joueur
		this.player.setNumberRamdomOffer(player.getNumberRamdomOffer());
		
		//Si le joueur ne peut plus faire de proposition 
		if(this.player.getNumberOffer() == 0) {
			String proposition = "Vous ne pouvez plus faire de proposition. Vous devez attendre la fin de la partie";
			this.player.getMessages().add(proposition);
			return true;
		}
		
		//Un joueur a fait une proposition, on décrémente le nombre de proposition du joueur
		this.player.decrementNumberOffer();
		
		//On affiche le nombre d'essais restant au joueur
		String essai = "Il reste " + this.player.getNumberOffer() + " essai(s)";
		this.player.getMessages().add(essai);
		
		//On notifie l'offre au reste des joueurs
		ServerUnknownNumberGame.getInstance().getServer().notifyAllObservers("NOTIFY_OFFER", this.player);
		
		//Si le joueur courant a trouvé le nombre mystère
		boolean win = this.player.getNumberRamdomOffer() == ServerUnknownNumberGame.getInstance().getNumberRamdom();
		
		//Si il n'y a plus de joueur pour faire une nouvelle proposition
		boolean playerCannotBeOffer = !ServerUnknownNumberGame.getInstance().playerCanBeOffer();
		
		/**
		 * Si le joueur courant a trouvé le nombre mystère ou
		 * Si il n'y a plus de joueur pour faire une nouvelle proposition
		 * Alors on relance une nouvelle partie
		 */
		if(win || playerCannotBeOffer) {
			//start new game fait appel à NOTIFY_INTERVAL
			ServerUnknownNumberGame.getInstance().startNewGame();
		} else {
			//On notifie le nouvel interval au reste des joueurs
			ServerUnknownNumberGame.getInstance().getServer().notifyAllObservers("NOTIFY_INTERVAL", null);
		}

		return true;
	}

	@Override
	public ServerPlayer receive(Object arg) {
		//On renvoit le joueur modifié dans la méthode "send"
		this.player.setCommand("REQUEST_OFFER");
		return this.player;
	}
}
