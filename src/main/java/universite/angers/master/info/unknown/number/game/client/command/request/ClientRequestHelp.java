package universite.angers.master.info.unknown.number.game.client.command.request;

import universite.angers.master.info.network.service.Commandable;
import universite.angers.master.info.unknown.number.game.client.ClientPlayer;

/**
 * Classe qui permet d'envoyer une requete au serveur pour afficher l'aide et les règles du jeu via "receive()"
 * et de recevoir une reponse dans le "send()"
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ClientRequestHelp implements Commandable<ClientPlayer> {

	private ClientPlayer player;
	
	public ClientRequestHelp(ClientPlayer player) {
		this.player = player;
	}
	
	@Override
	public boolean send(ClientPlayer player) {
		for(String message : player.getMessages()) {
			System.out.println("["+this.player.getName()+"] " + message);
		}
		return true;
	}

	@Override
	public ClientPlayer receive(Object arg) {
		this.player.setCommand("REQUEST_HELP");
		return this.player;
	}
}
