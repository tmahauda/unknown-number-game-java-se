package universite.angers.master.info.unknown.number.game.client;

/**
 * Test client
 * Attention il faut démarrer préalablement le serveur dans "ServerMain"
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ClientMain {
	
	public static void main(String[] args) {
        
		//TEST MULTI-CLIENT
		
		ClientUnknownNumberGame bob = new ClientUnknownNumberGame("Bob");
		ClientUnknownNumberGame alice = new ClientUnknownNumberGame("Alice");
		
        bob.getClient().open();
        /**
         * [Bob] Bienvenue sur le serveur du nombre inconnu Bob
		 * [Bob] Vous pouvez taper /help pour obtenir la liste des commandes valides et les règles du jeu
         */
        
        waitClient(1000);
        
        alice.getClient().open();
        /**
         * [Alice] Bienvenue sur le serveur du nombre inconnu Alice
		 * [Alice] Vous pouvez taper /help pour obtenir la liste des commandes valides et les règles du jeu
         */
        
        //Bob demande l'aide
        bob.registerRequestHelp();
        /**
         * [Bob] Voici la liste des commandes valides : 
			[Bob] REQUEST_OFFER
			[Bob] REQUEST_RANKING
			[Bob] REQUEST_NAME
			[Bob] REQUEST_DEFAULT
			[Bob] REQUEST_GAME
			[Bob] REQUEST_HELP
			[Bob] REQUEST_CLOSE
			[Bob] Voici les règles : 
			[Bob] Le serveur tire aléatoirement un nombre entre 1 et 1 000 000 sans le révéler aux joueurs
			[Bob] Chaque joueur présent peut faire jusqu'à 3 propositions, après quoi il devra attendre la fin de
			la partie. Si aucun joueur présent ne peut jouer (aucune proposition disponible) le serveur
			relance une nouvelle partie
			[Bob] À chaque proposition, le serveur envoi la proposition à tous les joueurs ainsi que l'intervalle
			mise à jour dans laquelle se trouve le nombre mystère
         */
        
        waitClient(1000);
        
		//Bob fait une offre à 100 000
        bob.registerRequestOffer(100000);
        /**
         * Bob propose 100000, le nombre est plus grand
         * Le nombre inconnu est compris entre 100000 et 1000000
         * Il reste 2 essai(s)
         */
        
        waitClient(1000);
        
        //Bob demande l'état de la partie
        bob.registerRequestGame();
        /**
         * Voici l'état de la partie : 
		 * Le nombre inconnu est compris entre 100000 et 1000000
		 * Il reste 2 essai(s) à Bob
         */
        
        //Bob demande le classement
        bob.registerRequestRanking();
        /**
         * Voici le classement du jeu : 
		 * Bob : 0 victoire(s)
         */
        
        waitClient(1000);
        
        //Bob fait une offre à 200 000
        bob.registerRequestOffer(200000);
        /**
         * Bob propose 200000, le nombre est plus grand
         * Le nombre inconnu est compris entre 200000 et 1000000
         * Il reste 1 essai(s)
         */
        
        waitClient(1000);
        
        //Bob demande l'état de la partie
        bob.registerRequestGame();
        /**
         * Voici l'état de la partie : 
		 * Le nombre inconnu est compris entre 200000 et 1000000
		 * Il reste 1 essai(s) à Bob
         */
        
        waitClient(1000);
        
        //Bob demande le classement
        bob.registerRequestRanking();
        /**
         * Voici le classement du jeu : 
		 * Bob : 0 victoire(s)
         */
        
        waitClient(1000);
        
        //Bob fait une offre à 300 000
        bob.registerRequestOffer(300000);
        /**
         * Bob propose 300000, le nombre est plus grand
         * Plus aucun joueur ne peut jouer, fin de partie
         * Le nombre inconnu était 500000
         * Il reste 0 essai(s)
         */
        
        waitClient(1000);
        
        //Bob demande l'état de la partie
        bob.registerRequestGame();
        /**
         * Voici l'état de la partie : 
		 * Le nombre inconnu est compris entre 1 et 1000000
		 * Il reste 3 essai(s) à Bob
         */
        
        waitClient(1000);
        
        //Bob demande le classement
        bob.registerRequestRanking();
        /**
         * Voici le classement du jeu : 
		 * Bob : 0 victoire(s)
         */
        
        waitClient(1000);
        
        //Bob quitte le jeu
        bob.registerRequestClose();
        /**
         * Bob vient de se déconnecter
         * À bientôt Bob
         */
        
        waitClient(5000);
        
        bob.getClient().close();
	}
	

	private static void waitClient(long time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
