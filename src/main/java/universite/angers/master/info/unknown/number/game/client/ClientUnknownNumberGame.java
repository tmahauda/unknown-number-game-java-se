package universite.angers.master.info.unknown.number.game.client;

import org.apache.log4j.Logger;
import com.google.gson.reflect.TypeToken;
import universite.angers.master.info.api.translater.json.TranslaterJsonFactory;
import universite.angers.master.info.api.translater.json.TranslaterJsonToObject;
import universite.angers.master.info.api.translater.json.TranslaterObjectToJson;
import universite.angers.master.info.network.client.Client;
import universite.angers.master.info.network.client.ClientService;
import universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock;
import universite.angers.master.info.network.server.Subject;
import universite.angers.master.info.unknown.number.game.client.command.notify.ClientNotifyDefault;
import universite.angers.master.info.unknown.number.game.client.command.request.ClientRequestClose;
import universite.angers.master.info.unknown.number.game.client.command.request.ClientRequestDefault;
import universite.angers.master.info.unknown.number.game.client.command.request.ClientRequestGame;
import universite.angers.master.info.unknown.number.game.client.command.request.ClientRequestHelp;
import universite.angers.master.info.unknown.number.game.client.command.request.ClientRequestName;
import universite.angers.master.info.unknown.number.game.client.command.request.ClientRequestOffer;
import universite.angers.master.info.unknown.number.game.client.command.request.ClientRequestRanking;

/**
 * Client du jeu du nombre inconnu
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ClientUnknownNumberGame {

	private static final Logger LOG = Logger.getLogger(ClientUnknownNumberGame.class);
	
	private static final String HOST = "127.0.0.1";
	private static final int PORT = 2345;
	
	/**
	 * Le client du jeu
	 */
	private Client<ClientPlayer> client;
	
	/**
	 * Le joueur du jeu
	 */
	private ClientPlayer player;
	
	public ClientUnknownNumberGame(String name) {		
		this.player = new ClientPlayer("", name, "", 0, 3, 0);
		
		//On crée le service
		ClientService<ClientPlayer> service = new ClientService<>(null, null, new ClientConverterCommandName(), new ReentrantWithoutReadWriteLock(true));	
		
		//On enregistre les notifications que peut recevoir le client
		//Ici il s'agit uniquement de faire des sorties consoles quelques soients
		//la notification. Pas de traitement spécifique pour chaque notification
		ClientNotifyDefault notifyDefault = new ClientNotifyDefault(this.player);
		service.registerNotification(Subject.NOTIFY_ECHO.getName(), notifyDefault);
		service.registerNotification(Subject.NOTIFY_CLOSE.getName(), notifyDefault);
        service.registerNotification(Subject.NOTIFY_HELLO.getName(), notifyDefault);
        service.registerNotification(Subject.NOTIFY_NEW.getName(), notifyDefault);
        service.registerNotification(Subject.NOTIFY_DEFAULT.getName(), notifyDefault);
        service.registerNotification("NOTIFY_GAME", notifyDefault);
        service.registerNotification("NOTIFY_OFFER", notifyDefault);
        service.registerNotification("NOTIFY_INTERVAL", notifyDefault);
        
        service.setNotificationDefault(notifyDefault);
        service.setRequestDefault(new ClientRequestDefault(this.player));
        
        //On enregistre une requete qui va donner le nom du joueur au serveur dés la connexion
        service.registerRequest("REQUEST_NAME", new ClientRequestName(this.player, name));
        
        //On créé les traducteurs pour convertir un objet en json et inversement
        
        //OBJECT --> JSON
        TranslaterObjectToJson<ClientPlayer> playerToJson = TranslaterJsonFactory.getTranslaterObjectToJson(new TypeToken<ClientPlayer>() {});
        
        //JSON --> OBJECT
        TranslaterJsonToObject<ClientPlayer> jsonToPlayer = TranslaterJsonFactory.getTranslaterJsonToObject(new TypeToken<ClientPlayer>() {});
        
        //On crée le client
        this.client = new Client<>(HOST, PORT, service, playerToJson, jsonToPlayer);
        
		//On crée le joueur
        String port = ""+this.client.getClientSocket().getLocalPort();
        LOG.debug("Port socket : " + port);
        
        String ip = this.client.getClientSocket().getLocalAddress().toString();
        LOG.debug("IP socket : " + ip);
        
		this.player.setIdentifiant(port+ip);
	}

	/**
	 * Requete qui permet d'afficher l'aide et les règles du jeu
	 * @return
	 */
	public boolean registerRequestHelp() {
		return this.client.getClientService().registerRequest("REQUEST_HELP", new ClientRequestHelp(this.player));
	}
	
	/**
	 * Requete qui permet de faire une proposition
	 * @param offer la proposition
	 * @return
	 */
	public boolean registerRequestOffer(int offer) {
		return this.client.getClientService().registerRequest("REQUEST_OFFER", new ClientRequestOffer(this.player, offer));
	}
	
	/**
	 * Requete qui permet d'afficher l'état de la partie en cours, qu'il sera le seul à voir.
	 * @return
	 */
	public boolean registerRequestGame() {
		return this.client.getClientService().registerRequest("REQUEST_GAME", new ClientRequestGame(this.player));
	}
	
	/**
	 * Requete qui permet d'afficher le classement serveur, qu'il sera le seul à voir.
	 * @return
	 */
	public boolean registerRequestRanking() {
		return this.client.getClientService().registerRequest("REQUEST_RANKING", new ClientRequestRanking(this.player));
	}
	
	/**
	 * Requete qui permet de quitter, ce qui avertira les autres joueurs de son départ.
	 * @return
	 */
	public boolean registerRequestClose() {
		return this.client.getClientService().registerRequest("REQUEST_CLOSE", new ClientRequestClose(this.player));
	}
	
	/**
	 * @return the client
	 */
	public Client<ClientPlayer> getClient() {
		return client;
	}

	/**
	 * @param client the client to set
	 */
	public void setClient(Client<ClientPlayer> client) {
		this.client = client;
	}

	/**
	 * @return the player
	 */
	public ClientPlayer getPlayer() {
		return player;
	}

	/**
	 * @param player the player to set
	 */
	public void setPlayer(ClientPlayer player) {
		this.player = player;
	}

	@Override
	public String toString() {
		return "ClientUnknownNumberGame [client=" + client + ", player=" + player + "]";
	}
}
