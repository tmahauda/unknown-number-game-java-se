package universite.angers.master.info.unknown.number.game.server.command.notify;

import universite.angers.master.info.network.service.Commandable;
import universite.angers.master.info.unknown.number.game.server.ServerPlayer;
import universite.angers.master.info.unknown.number.game.server.ServerUnknownNumberGame;

/**
 * Classe qui permet d'afficher l'intervalle mise à jour dans laquelle se trouve le nombre mystère
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ServerNotifyInterval implements Commandable<ServerPlayer> {

	@Override
	public boolean send(ServerPlayer player) {
		System.out.println(player);
		return true;
	}

	@Override
	public ServerPlayer receive(Object arg) {
		ServerPlayer playerMessage = new ServerPlayer();
		
		//Affiche l'intervalle mise à jour dans laquelle se trouve le nombre mystère
		String interval = "Le nombre inconnu est compris entre " + 
				ServerUnknownNumberGame.getInstance().getNumberRandomInf() + " et " +
				ServerUnknownNumberGame.getInstance().getNumberRandomSup();

		playerMessage.getMessages().add(interval);
		playerMessage.setCommand("NOTIFY_INTERVAL");
		return playerMessage;
	}
}
