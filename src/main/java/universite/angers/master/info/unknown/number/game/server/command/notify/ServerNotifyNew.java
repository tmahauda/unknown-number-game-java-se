package universite.angers.master.info.unknown.number.game.server.command.notify;

import universite.angers.master.info.network.server.Subject;
import universite.angers.master.info.network.service.Commandable;
import universite.angers.master.info.unknown.number.game.server.ServerPlayer;

/**
 * Classe qui permet d'afficher une nouvelle connexion d'un client
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ServerNotifyNew implements Commandable<ServerPlayer> {

	@Override
	public boolean send(ServerPlayer player) {
		System.out.println(player);
		return true;
	}

	@Override
	public ServerPlayer receive(Object arg) {
		ServerPlayer player = (ServerPlayer)arg;
		ServerPlayer playerMessage = new ServerPlayer();
		
		//Affiche un message de connexion
		String connect = player.getName() + " vient de se connecter";
		
		playerMessage.getMessages().add(connect);
		playerMessage.setCommand(Subject.NOTIFY_NEW.getName());
		return playerMessage;
	}
}
