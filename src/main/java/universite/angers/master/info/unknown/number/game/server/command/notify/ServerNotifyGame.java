package universite.angers.master.info.unknown.number.game.server.command.notify;

import universite.angers.master.info.network.service.Commandable;
import universite.angers.master.info.unknown.number.game.server.ServerPlayer;

/**
 * Classe qui permet d'afficher une nouvelle partie
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ServerNotifyGame implements Commandable<ServerPlayer> {

	@Override
	public boolean send(ServerPlayer player) {
		System.out.println(player);
		return true;
	}

	@Override
	public ServerPlayer receive(Object arg) {
		ServerPlayer playerMessage = new ServerPlayer();
		
		//Affiche une nouvelle partie
		String game = "Début de nouvelle partie";

		playerMessage.getMessages().add(game);
		playerMessage.setCommand("NOTIFY_GAME");
		return playerMessage;
	}
}
