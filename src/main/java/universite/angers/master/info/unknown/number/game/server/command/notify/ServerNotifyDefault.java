package universite.angers.master.info.unknown.number.game.server.command.notify;

import universite.angers.master.info.network.server.Subject;
import universite.angers.master.info.network.service.Commandable;
import universite.angers.master.info.unknown.number.game.server.ServerPlayer;

/**
 * Classe qui permet d'afficher une notification inconnu
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ServerNotifyDefault implements Commandable<ServerPlayer> {
	
	@Override
	public boolean send(ServerPlayer player) {
		System.out.println(player);
		return true;
	}

	@Override
	public ServerPlayer receive(Object arg) {
		ServerPlayer playerMessage = new ServerPlayer();
		
		playerMessage.getMessages().add("Notification inconnu");
		
		playerMessage.setCommand(Subject.NOTIFY_DEFAULT.getName());
		return playerMessage;
	}
}
