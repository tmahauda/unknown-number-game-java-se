package universite.angers.master.info.unknown.number.game.server;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import com.google.gson.reflect.TypeToken;
import universite.angers.master.info.api.translater.json.TranslaterJsonFactory;
import universite.angers.master.info.api.translater.json.TranslaterJsonToObject;
import universite.angers.master.info.api.translater.json.TranslaterObjectToJson;
import universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock;
import universite.angers.master.info.network.server.Server;
import universite.angers.master.info.network.server.ServerService;
import universite.angers.master.info.network.server.Subject;
import universite.angers.master.info.unknown.number.game.server.command.notify.ServerNotifyClose;
import universite.angers.master.info.unknown.number.game.server.command.notify.ServerNotifyDefault;
import universite.angers.master.info.unknown.number.game.server.command.notify.ServerNotifyEcho;
import universite.angers.master.info.unknown.number.game.server.command.notify.ServerNotifyGame;
import universite.angers.master.info.unknown.number.game.server.command.notify.ServerNotifyInterval;
import universite.angers.master.info.unknown.number.game.server.command.notify.ServerNotifyNew;
import universite.angers.master.info.unknown.number.game.server.command.notify.ServerNotifyOffer;
import universite.angers.master.info.unknown.number.game.server.command.request.ServerRequestClose;
import universite.angers.master.info.unknown.number.game.server.command.request.ServerRequestDefault;
import universite.angers.master.info.unknown.number.game.server.command.request.ServerRequestGame;
import universite.angers.master.info.unknown.number.game.server.command.request.ServerRequestHelp;
import universite.angers.master.info.unknown.number.game.server.command.request.ServerRequestName;
import universite.angers.master.info.unknown.number.game.server.command.request.ServerRequestOffer;
import universite.angers.master.info.unknown.number.game.server.command.request.ServerRequestRanking;

/**
 * Serveur du jeu du nombre inconnu
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ServerUnknownNumberGame {

	private static final String HOST = "127.0.0.1";
	
	private static final int PORT = 2345;

	private static final int NUMBER_RANDOM_INF = 1;
	
	private static final int NUMBER_RANDOM_SUP = 1000000;

	/**
	 * Le nombre de proposition que peut faire chaque joueur pour deviner le nombre mistère
	 */
	private static final int NUMBER_OFFER_TO_GUESS_NUMBER_RANDOM_PER_PLAYER = 3;
	
	/**
	 * Une unique instance du jeu
	 */
	private static ServerUnknownNumberGame serverUnknownNumberGame;
	
	/**
	 * Le serveur du jeu
	 */
	private Server<ServerPlayer> server;
	
	/**
	 * Les joueurs enregistrés
	 */
	private Map<String, ServerPlayer> players;
	
	/**
	 * Le nombre minimum que peut tirer le serveur
	 */
	private int numberRandomInf;
	
	/**
	 * Le nombre tiré aléatoirement par le serveur entre number inf et sup
	 */
	private int numberRamdom;
	
	/**
	 * Le nombre maximum que peut tirer le serveur
	 */
	private int numberRandomSup;
	
	private ServerUnknownNumberGame() {
		this.players = new HashMap<>();
		
		//Service
		ServerService<ServerPlayer> service = new ServerService<>(null, null, new ServerConverterCommandName(), new ReentrantWithoutReadWriteLock(true));	

		//On enregistre les notifications à envoyer aux clients sans retour de réponse
		service.registerNotification(Subject.NOTIFY_ECHO.getName(), new ServerNotifyEcho());
		service.registerNotification(Subject.NOTIFY_CLOSE.getName(), new ServerNotifyClose());
        service.registerNotification(Subject.NOTIFY_NEW.getName(), new ServerNotifyNew());
        service.registerNotification(Subject.NOTIFY_DEFAULT.getName(), new ServerNotifyDefault());
        service.registerNotification("NOTIFY_GAME", new ServerNotifyGame());
        service.registerNotification("NOTIFY_OFFER", new ServerNotifyOffer());
        service.registerNotification("NOTIFY_INTERVAL", new ServerNotifyInterval());
		
		//On enregistre les requetes venant du client qui pourront etre traité par le serveur
        service.registerRequest("REQUEST_CLOSE", new ServerRequestClose());
        service.registerRequest("REQUEST_GAME", new ServerRequestGame());
        service.registerRequest("REQUEST_HELP", new ServerRequestHelp());
        service.registerRequest("REQUEST_OFFER", new ServerRequestOffer());
        service.registerRequest("REQUEST_RANKING", new ServerRequestRanking());
        service.registerRequest("REQUEST_NAME", new ServerRequestName());
        service.registerRequest("REQUEST_DEFAULT", new ServerRequestDefault());
        
        service.setRequestDefault(new ServerRequestDefault());
        service.setNotificationDefault(new ServerNotifyDefault());
        
        //On passe les traducteurs pour convertir un objet en json et inversement
        
        //OBJECT --> JSON
        TranslaterObjectToJson<ServerPlayer> playerToJson = TranslaterJsonFactory.getTranslaterObjectToJson(new TypeToken<ServerPlayer>() {});
        
        //JSON --> OBJECT
        TranslaterJsonToObject<ServerPlayer> jsonToPlayer = TranslaterJsonFactory.getTranslaterJsonToObject(new TypeToken<ServerPlayer>() {});
        
        this.server = new Server<>(HOST, PORT, 10, service, playerToJson, jsonToPlayer);
        
        this.startNewGame();
	}

	public static synchronized ServerUnknownNumberGame getInstance() {
		if(serverUnknownNumberGame == null) {
			serverUnknownNumberGame = new ServerUnknownNumberGame();	
		}
		
		return serverUnknownNumberGame;
	}

	public ServerPlayer getOrNewPlayer(String id) {
		ServerPlayer player = null;
		if(!this.players.containsKey(id)) {
			player = new ServerPlayer(id, "", "", 0, NUMBER_OFFER_TO_GUESS_NUMBER_RANDOM_PER_PLAYER, 0);
			this.players.put(id, player);
		} else {
			player = this.players.get(id);
			player.getMessages().clear();
		}
		
		return player;
	}
	
	/**
	 * Vérifie si au moins un joueur peut faire une proposition pour deviner le nombre mystère
	 * @return
	 */
	public boolean playerCanBeOffer() {
		for(ServerPlayer player : this.players.values()) {
			//Si au moins un joueur peut faire une proposition dans ce cas le jeu
			//peut continuer
			if(player.getNumberOffer() > 0)
				return true;
		}
		
		return false;
	}
	
	public void startNewGame() {
		for(ServerPlayer player : this.players.values()) {
			//On réinitialise le nombre de proposition pour chaque joueur
			//avant de commencer une nouvelle partie
			player.setNumberOffer(NUMBER_OFFER_TO_GUESS_NUMBER_RANDOM_PER_PLAYER);
		}
		
		this.numberRandomInf = NUMBER_RANDOM_INF;
		this.numberRandomSup = NUMBER_RANDOM_SUP;
		this.numberRamdom = this.generateNumberRandom(NUMBER_RANDOM_INF, NUMBER_RANDOM_SUP);
		System.out.println("Nombre mystère : " + this.numberRamdom);
		
		//On déclare une nouvelle partie
		this.server.notifyAllObservers("NOTIFY_GAME", null);
		
		//On déclare l'interval pour trouver le nouveau nombre
		this.server.notifyAllObservers("NOTIFY_INTERVAL", null);
	}
	
	public boolean removePlayer(ServerPlayer player) {
		//Suppression coté jeu
		this.players.remove(player.getIdentifiant());
		
		//On notifie les autres joueurs que le joueur courant est pret à se déconnecter
		this.server.notifyAllObservers(Subject.NOTIFY_CLOSE.getName(), player);
		
		return true;
	}
	
	/**
	 * Générer un nombre mystère entre min et max inclus
	 * @param min
	 * @param max
	 * @return
	 */
	private int generateNumberRandom(int min, int max) {
		return new Random().nextInt((max - min) + 1) + min;
	}
	
	/**
	 * @return the server
	 */
	public Server<ServerPlayer> getServer() {
		return server;
	}

	/**
	 * @param server the server to set
	 */
	public void setServer(Server<ServerPlayer> server) {
		this.server = server;
	}

	/**
	 * @return the numberRandomInf
	 */
	public int getNumberRandomInf() {
		return numberRandomInf;
	}

	/**
	 * @param numberRandomInf the numberRandomInf to set
	 */
	public void setNumberRandomInf(int numberRandomInf) {
		this.numberRandomInf = numberRandomInf;
	}

	/**
	 * @return the numberRamdom
	 */
	public int getNumberRamdom() {
		return numberRamdom;
	}

	/**
	 * @param numberRamdom the numberRamdom to set
	 */
	public void setNumberRamdom(int numberRamdom) {
		this.numberRamdom = numberRamdom;
	}

	/**
	 * @return the numberRandomSup
	 */
	public int getNumberRandomSup() {
		return numberRandomSup;
	}

	/**
	 * @param numberRandomSup the numberRandomSup to set
	 */
	public void setNumberRandomSup(int numberRandomSup) {
		this.numberRandomSup = numberRandomSup;
	}
	
	/**
	 * @return the players
	 */
	public Map<String, ServerPlayer> getPlayers() {
		return players;
	}

	/**
	 * @param players the players to set
	 */
	public void setPlayers(Map<String, ServerPlayer> players) {
		this.players = players;
	}

	@Override
	public String toString() {
		return "ServerUnknownNumberGame [server=" + server + ", numberRandomInf=" + numberRandomInf + ", numberRamdom="
				+ numberRamdom + ", numberRandomSup=" + numberRandomSup + "]";
	}
}
