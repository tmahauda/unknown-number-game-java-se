package universite.angers.master.info.unknown.number.game.server;

import java.util.ArrayList;
import java.util.List;

/**
 * La classe joueur du serveur
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ServerPlayer {

	/**
	 * L'identifiant unique du joueur, qui est le localport de la socket
	 */
	private String identifiant;
	
	/**
	 * Le nom du joueur
	 */
	private String name;
	
	/**
	 * Les messages du serveur
	 */
	private List<String> messages;
	
	/**
	 * La commande courante du joueur
	 */
	private String command;
	
	/**
	 * La proposition du joueur
	 */
	private int numberRamdomOffer;
	
	/**
	 * Le nombre d'essai qu'il lui reste
	 */
	private int numberOffer;
	
	/**
	 * Le nombre de partie gagné
	 */
	private int numberGameWin;

	/**
	 * Attention ! penser à fournir un constructeur par défaut pour le convertisseur Json
	 */
	public ServerPlayer() {
		this("", "", "", 0, 0, 0);
	}
	
	public ServerPlayer(String identifiant, String name, String command, 
			int numberRamdomOffer, int numberOffer, int numberGameWin) {
		this.identifiant = identifiant;
		this.name = name;
		this.messages = new ArrayList<>();
		this.command = command;
		this.numberRamdomOffer = numberRamdomOffer;
		this.numberOffer = numberOffer;
		this.numberGameWin = numberGameWin;
	}
	
	public void incrementNumberGameWin() {
		this.numberGameWin++;
	}
	
	public void decrementNumberGameWin() {
		if(this.numberGameWin > 0) {
			this.numberGameWin--;
		}
	}
	
	public void incrementNumberOffer() {
		this.numberOffer++;
	}
	
	public void decrementNumberOffer() {
		if(this.numberOffer > 0) {
			this.numberOffer--;
		}
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the numberRamdomOffer
	 */
	public int getNumberRamdomOffer() {
		return numberRamdomOffer;
	}

	/**
	 * @param numberRamdomOffer the numberRamdomOffer to set
	 */
	public void setNumberRamdomOffer(int numberRamdomOffer) {
		this.numberRamdomOffer = numberRamdomOffer;
	}

	/**
	 * @return the numberOffer
	 */
	public int getNumberOffer() {
		return numberOffer;
	}

	/**
	 * @param numberOffer the numberOffer to set
	 */
	public void setNumberOffer(int numberOffer) {
		this.numberOffer = numberOffer;
	}

	/**
	 * @return the command
	 */
	public String getCommand() {
		return command;
	}

	/**
	 * @param command the command to set
	 */
	public void setCommand(String command) {
		this.command = command;
	}

	/**
	 * @return the messages
	 */
	public List<String> getMessages() {
		return messages;
	}

	/**
	 * @param messages the messages to set
	 */
	public void setMessages(List<String> messages) {
		this.messages = messages;
	}

	/**
	 * @return the identifiant
	 */
	public String getIdentifiant() {
		return identifiant;
	}

	/**
	 * @param identifiant the identifiant to set
	 */
	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}

	/**
	 * @return the numberGameWin
	 */
	public int getNumberGameWin() {
		return numberGameWin;
	}

	/**
	 * @param numberGameWin the numberGameWin to set
	 */
	public void setNumberGameWin(int numberGameWin) {
		this.numberGameWin = numberGameWin;
	}

	@Override
	public String toString() {
		return "ServerPlayer [identifiant=" + identifiant + ", name=" + name + ", messages=" + messages + ", command="
				+ command + ", numberRamdomOffer=" + numberRamdomOffer + ", numberOffer=" + numberOffer
				+ ", numberGameWin=" + numberGameWin + "]";
	}
}
